from flask import Flask, request

import subprocess
app = Flask(__name__)


@app.route('/')
def hello():
    return 'This is just a test to check that CI/CD with webhooks works fine'

@app.route('/update-server', methods=['POST'])
def webhook():
    if request.method == 'POST':
        subprocess.check_output(["./update_repo.sh"], shell=True)
        return 'Updated Toolforge project successfully', 200
    else:
        return 'Wrong event type', 400

if __name__=="__main__":
    app.run(debug=True)
